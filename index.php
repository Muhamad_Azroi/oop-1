

<?php


// require('Animal.php');
require('frog.php');
require('ape.php');


$sheep = new Animal("shaun");

echo "Nama : ". $sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "Cold Blooded : ".$sheep->cold_blooded. "<br><br>"; // "no"

$buduk = new frog("buduk");

echo "Nama : ". $buduk->name."<br>"; // "shaun"
echo "Legs : ".$buduk->legs."<br>"; // 4
echo "Cold Blooded : ".$buduk->cold_blooded."<br>"; // "no"
echo $buduk->jump(). "<br><br>";

$kera = new ape("kera sakti");

echo "Nama : ". $kera->name."<br>"; // "shaun"
echo "Legs : ".$kera->legs."<br>"; // 4
echo "Cold Blooded : ".$kera->cold_blooded."<br>"; // "no"
echo $kera->yell(). "<br><br>";